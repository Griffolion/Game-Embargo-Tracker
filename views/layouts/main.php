<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;
use kartik\icons\Icon;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php Icon::map($this, Icon::WHHG);?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Game Embargo Tracker',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
                echo Typeahead::widget([
                    'id' => 'search_all',
                    'name' => 'search_all',
                    'container' => [
                        'class' => 'col-lg-7',
                        'style' => 'position: relative; top: -25%; transform: translateY(25%);'
                    ],
                    'options' => ['placeholder' => 'Search All'],
                    'scrollable' => false,
                    'pluginOptions' => ['highlight'=>false],
                    'pluginEvents' => [
                        'typeahead:selected' => 'getData',
                    ],
                    'dataset' => [
                        [
                            'remote' => Url::to(['game/getgames']) . '?q=%QUERY',
                            'limit' => 10,
                            'templates' => [
                                'header' =>  '<h4 style="padding-left: 3px;">' . Icon::show('controllersnes', ['class' => 'fa-3x'], Icon::WHHG) . '<b>Games</b></h4>',
                                'empty' => '<p style="padding-left:20px;">No Games Match</p>'
                            ],
                            'displayKey' => 'game',
                        ],
                        [
                            'remote' => Url::to(['publisher/getpublishers']) . '?q=%QUERY',
                            'limit' => 10,
                            'templates' => [
                                'header' => '<h4 style="padding-left: 3px;">' . Icon::show('dollar', ['class' => 'fa-3x'], Icon::WHHG) . '<b>  Publishers</b></h4>',
                                'empty' => '<p style="padding-left:20px;">No Publishers Match</p>'
                            ],
                            'displayKey' => 'publisher',
                        ],
                        [
                            'remote' => Url::to(['developer/getdevelopers']) . '?q=%QUERY',
                            'limit' => 10,
                            'templates' => [
                                'header' => '<h4 style="padding-left: 3px;">' . Icon::show('parentheses', ['class' => 'fa-3x'], Icon::WHHG) . '<b>  Developers</b></h4>',
                                'empty' => '<p style="padding-left:20px;">No Developers Match</p>'
                            ],
                            'displayKey' => 'developer',
                        ],
                        /*[
                            'remote' => Url::to(['platform/getplatforms']) . '?q=%QUERY',
                            'limit' => 10,
                            'templates' => [
                                'header' => '<h4 style="padding-left: 3px;">' . Icon::show('controllersnes', ['class' => 'fa-3x'], Icon::WHHG) . '<b>  Platforms<b></h4>',
                                'empty' => '<p style="padding-left:20px;">No Developers Match</p>'
                            ],
                            'displayKey' => 'platform',
                        ],*/
                    ]
                ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'About', 'url' => ['/site/about']],
                    //['label' => 'Contact', 'url' => ['/site/contact']],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Admin Login', 'url' => ['/site/login']]
                    : 
                        ['label' => 'Admin Panel', 'items' => [
                            ['label' => 'Developers', 'url' => ['/developer/index']],
                            ['label' => 'Games', 'url' => ['/game/index']],
                            ['label' => 'Game <> Developer Links', 'url' => ['/gamedev/index']],
                            ['label' => 'Game <> Platform Links', 'url' => ['/gameplatform/index']],
                            ['label' => 'Game Reviews', 'url' => ['/gamereview/index']],
                            ['label' => 'Hardware Vendors', 'url' => ['/hardwarevendor/index']],
                            ['label' => 'Platforms', 'url' => ['/platform/index']],
                            ['label' => 'Publishers', 'url' => ['/publisher/index']],
                            ['label' => 'Reviewers', 'url' => ['/reviewer/index']],
                            ['label' => 'New User', 'url' => ['/site/register']],
                            ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post'],
                                'options' => ['style' => 'background-color:#FF4D4D']]
                        ]],
                ],
            ]);
            NavBar::end();
        ?>
        
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <!--<p class="pull-left">&copy; Embargo <?//= date('Y') ?></p>-->
            <p style="text-align: center;"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>

<script>
   ( function( $ ){

	$.redirect = function( target, values, method ) {  

		method = (method && method.toUpperCase() == 'GET') ? 'GET' : 'POST';
			
		if (!values)
		{
			var obj = $.parse_url(target);
			target = obj.url;
			values = obj.params;
		}
					
		var form = $('<form>',{attr:{
			method: method,
			action: target
		}});
		
		for(var i in values)
		{
			$('<input>',{
				attr:{
					type: 'hidden',
					name: i,
					value: values[i]
				}
			}).appendTo(form);

		}
		
		$('body').append(form);
        console.log(form);
		form.submit();
	};
	
	$.parse_url = function(url)
	{
		if (url.indexOf('?') == -1)
			return { url: url, params: {} }
			
		var parts = url.split('?');
		var url = parts[0];
		var query_string = parts[1];
		
		var return_obj = {};
		var elems = query_string.split('&');
		
		var obj = {};
		
		for(var i in elems)
		{
			var elem = elems[i];
			var pair = elem.split('=');
			obj[pair[0]] = pair[1];
		}
		
		return_obj.url = url;
		return_obj.params = obj;
		
		return return_obj;		
	}  	
})( jQuery );

    function getData(event, datum, dataset) {
        event.preventDefault();
        var Controller = '<?= preg_replace('/\/.*/', '', preg_replace('/\/.*\/web\//', '', Yii::$app->request->url)) ?>';
        var baseSection = '<?= preg_replace('/\/web\/.*/', '', Yii::$app->request->url) ?>';
        var Key;
        for (var k in datum) {
            Key = k;
        }
        if (Key === Controller) {
            var req = $.ajax( {
                type: 'POST',
                url: 'getchilddata',
                data: { data: datum[Key] },
                })
            .done(function(data) {
                $('#display-div').html(data);
                document.getElementById('search_all').value = '';
            })
            .fail(function() {
                console.log("Failed");
                document.getElementById('search_all').value = '';
            })
        } else {
            $.redirect(baseSection + '/web/' + Key + '/datastream', {q: datum[Key], _csrf: $('meta[name="csrf-token"]').attr("content")}, 'POST');
        }
    }
</script>