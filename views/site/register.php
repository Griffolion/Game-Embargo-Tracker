<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */
/* @var $success boolean */

$this->title = 'Register User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <?php if (isset($success)) {
        echo '<div class="alert alert-info" role="success"><strong>User Successfully Created</strong></div>';
    } ?>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to register a user:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'register-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    
    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>
    
    <?= $form->field($model, 'passwordVerify')->passwordInput() ?>
    
    <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default', 'name' => 'reset-button']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
