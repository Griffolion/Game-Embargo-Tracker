<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?> Game Embargo Tracker</h1>

    <h4>
        This site intends to track differences between a games release date and
        its review embargo date. If a studio develops a game that is likely to review
        poorly, it may be in their interest to delay the review embargo until 
        after release. This way, the consumer has no way to know that what they
        are buying may be of poor quality, and thus have no warning to skip
        purchasing the game. By keeping a track record of each game's release dates
        and review embargo dates, you can see how likely a publisher or studio is
        to screw you over if they do not release the review embargo at an appropriate
        time.
    </h4>
    
    <div style="text-align: center">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/gavcvi19q1g" frameborder="0" allowfullscreen></iframe>
    </div>

</div>
