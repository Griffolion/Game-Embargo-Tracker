<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Game Embargo Tracker';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Game Embargo Tracker</h1>

        <p class="lead">Giving you insight into who in the games industry is abusing review embargoes.</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>By Game</h2>

                <p>See embargo statistics according to each individual game.</p>

                <?= Html::a('Search by Game &raquo', ['game/datastream'], ['class' => 'btn btn-default']) ?>
            </div>
            <div class="col-lg-4">
                <h2>By Studio</h2>

                <p>See embargo statistics according to the studio that developed the game.</p>

                <?= Html::a('Search by Studio &raquo', ['developer/datastream'], ['class' => 'btn btn-default']) ?>
            </div>
            <div class="col-lg-4">
                <h2>By Publisher</h2>

                <p>See embargo statistics according to the publishing company of the game.</p>

                <?= Html::a('Search by Publisher &raquo', ['publisher/datastream'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>