<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Platform */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="platform-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Hardware_Vendor')->widget(Typeahead::className(),[
        'options' => ['placeholder' => 'Type hardware vendor name here'],
        'dataset' => [[
            'remote' => Url::to(['hardwarevendor/gethardwarevendors']) . '?q=%QUERY',
            'limit' => 10,
            'displayLink' => 'value',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]) ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => 256]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
