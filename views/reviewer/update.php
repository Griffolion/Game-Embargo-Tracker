<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reviewer */

$this->title = 'Update Reviewer: ' . ' ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Reviewers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reviewer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
