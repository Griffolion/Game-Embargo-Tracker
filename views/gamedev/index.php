<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GamedevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Game <> Developer Links';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gamedev-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gamedev', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Game',
                'content' => function($model, $key, $index, $column){
                   return \app\models\Game::findOne(['ID' => $model->Game])->Name;
                   },
                'label' => 'Game'
            ],
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Developer',
                'content' => function($model, $key, $index, $column){
                   return \app\models\Developer::findOne(['ID' => $model->Developer])->Name;
                   },
                'label' => 'Developer'
            ],

            ['class' => 'yii\grid\ActionColumn',],
        ],
    ]); ?>

</div>
