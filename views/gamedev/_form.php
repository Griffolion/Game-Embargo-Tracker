<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Gamedev */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gamedev-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'Game')->widget(Typeahead::className(), [
        'options' => ['placeholder' => 'Type game name here'],
        'dataset' => [[
            'remote' => Url::to(['game/getgames']) . '?q=%QUERY',
            'limit' => 10,
            'displayLink' => 'value',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]); ?>
    
    <?= $form->field($model, 'Developer')->widget(Typeahead::className(), [
        'options' => ['placeholder' => 'Type developer name here'],
        'dataset' => [[
            'remote' => Url::to(['developer/getdevelopers']) . '?q=%QUERY',
            'limit' => 10,
            'displayLink' => 'value',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
