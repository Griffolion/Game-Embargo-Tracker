<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gamedev */

$this->title = 'Update Gamedev: ' . ' ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Gamedevs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gamedev-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
