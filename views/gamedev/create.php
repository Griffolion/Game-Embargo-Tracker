<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Gamedev */

$this->title = 'Create a Game <> Developer Link';
$this->params['breadcrumbs'][] = ['label' => 'Gamedevs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gamedev-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
