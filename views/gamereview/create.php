<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GameReview */

$this->title = 'Create Game Review';
$this->params['breadcrumbs'][] = ['label' => 'Game Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-review-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
