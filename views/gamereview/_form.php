<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\GameReview */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Reviewer')->widget(Typeahead::className(),[
        'options' => ['placeholder' => 'Type reviewer name here'],
        'dataset' => [[
            'remote' => Url::to(['reviewer/getreviewers']) . '?q=%QUERY',
            'limit' => 10,
            'displayLink' => 'value',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]) ?>

    <?= $form->field($model, 'Game')->widget(Typeahead::className(),[
        'options' => ['placeholder' => 'Type game name here'],
        'dataset' => [[
            'remote' => Url::to(['game/getgames']) . '?q=%QUERY',
            'limit' => 10,
            'displayLink' => 'value',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]) ?>
    
    <?= $form->field($model, 'Platform')->widget(Typeahead::className(),[
        'options' => ['placeholder' => 'Type platform name here'],
        'dataset' => [[
            'remote' => Url::to(['platform/getplatforms']) . '?q=%QUERY',
            'limit' => 10,
            'displayLink' => 'value',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]) ?>

    <?= $form->field($model, 'Score')->textInput() ?>

    <?= $form->field($model, 'Summary')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
