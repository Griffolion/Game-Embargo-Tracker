<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GameReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Game Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-review-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Game Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Reviewer',
                'label' => 'Reviewer',
                'content' => function($model, $key, $index, $column){
                        return $model->reviewer->Name;
                   },
            ],
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Game',
                'label' => 'Game',
                'content' => function($model, $key, $index, $column){
                        return $model->game->Name;
                   },
            ],
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Platform',
                'label' => 'Platform',
                'content' => function($model, $key, $index, $column){
                        return $model->platform->Name;
                   },
            ],
            'Score',
            'Summary:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
