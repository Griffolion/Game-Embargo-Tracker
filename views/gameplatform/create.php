<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Gameplatform */

$this->title = 'Create Gameplatform';
$this->params['breadcrumbs'][] = ['label' => 'Gameplatforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gameplatform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
