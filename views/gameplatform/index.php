<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GameplatformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Game <> Platform Links';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gameplatform-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gameplatform', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Game',
                'label' => 'Game',
                'content' => function($model, $key, $index, $column){
                        return $model->game->Name;
                   },
            ],
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Platform',
                'label' => 'Platform',
                'content' => function($model, $key, $index, $column){
                        return $model->platform->Name;
                   },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
