<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Gameplatform */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gameplatform-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Game')->widget(Typeahead::className(), [
        'options' => ['placeholder' => 'Type game name here'],
        'dataset' => [[
            'remote' => Url::to(['game/getgames']) . '?q=%QUERY',
            'limit' => 10,
            'displayKey' => 'game',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]); ?>

    <?= $form->field($model, 'Platform')->widget(Typeahead::className(), [
        'options' => ['placeholder' => 'Type platform name here'],
        'dataset' => [[
            'remote' => Url::to(['platform/getplatforms']) . '?q=%QUERY',
            'limit' => 10,
            'displayKey' => 'platform',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
