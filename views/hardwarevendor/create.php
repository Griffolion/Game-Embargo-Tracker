<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HardwareVendor */

$this->title = 'Create Hardware Vendor';
$this->params['breadcrumbs'][] = ['label' => 'Hardware Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hardware-vendor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
