<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HardwareVendor */

$this->title = 'Update Hardware Vendor: ' . ' ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Hardware Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hardware-vendor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
