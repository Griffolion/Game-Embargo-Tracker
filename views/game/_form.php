<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\typeahead\Typeahead;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'Name')->textInput(['maxlength' => 256]) ?>
    
    <?= $form->field($model, 'Publisher')->widget(Typeahead::className(), [
        'options' => ['placeholder' => 'Type publisher name here'],
        'dataset' => [[
            'remote' => Url::to(['publisher/getpublishers']) . '?q=%QUERY',
            'limit' => 10,
            'displayKey' => 'publisher',
        ]],
        'pluginOptions' => ['highlight' => true],
    ]); ?>
    
    <?= $form->field($model, 'Release_Date')->widget(DateTimePicker::className(), [
        'name' => 'release_picker',
        'type' => DateTimePicker::TYPE_INPUT,
        'options' => [
            'placeholder' => 'Pick Release Date'
        ],
        'pluginOptions' => [
            'language' => 'en',
            'autoclose' => true,
            'format' => 'yyyy-m-d hh:ii'
        ],
    ]); ?>
    
    <?= $form->field($model, 'Embargo_Date')->widget(DateTimePicker::className(), [
        'name' => 'embargo_picker',
        'type' => DateTimePicker::TYPE_INPUT,
        'options' => [
            'placeholder' => 'Pick Embargo Date'
        ],
        'pluginOptions' => [
            'language' => 'en',
            'autoclose' => true,
            'format' => 'yyyy-m-d hh:ii'
        ],
    ]); ?>
    
    <?= $form->field($model, 'Media_Date')->widget(DateTimePicker::className(), [
        'name' => 'media_picker',
        'type' => DateTimePicker::TYPE_INPUT,
        'options' => [
            'placeholder' => 'Pick Media Date'
        ],
        'pluginOptions' => [
            'language' => 'en',
            'autoclose' => true,
            'format' => 'yyyy-m-d hh:ii'
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
