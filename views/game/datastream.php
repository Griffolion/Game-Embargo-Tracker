<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kartik\typeahead\Typeahead;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $query string */
/* @var $Details Game */

$this->title = "Datastream: Game";
?>

<div id="display-div">
    <?php
        if (NULL !== $Details) {
            echo $this->render('_details', [
                'Details' => $Details
            ]);
        }
     ?>
</div>