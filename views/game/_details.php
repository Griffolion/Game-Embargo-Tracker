<?php
use app\models\Game;
use yii\web\View;
use yii\helpers\Html;

/* @var $Details Game */
/* @var $this yii\web\View */
?>

<div id="detail-view-master" class="col-lg-12">
    <h1 style="text-align: center">
        <?= $Details->Name; ?>
    </h1>
    
    <hr>
    
    <div id='pub-view' class='col-lg-4'>
        <h3> <?= Html::a($Details->publisher->Name, ['publisher/datastream', 'q' => $Details->publisher->Name]) ?> </h3>
    </div>
    
    <div id='dev-view' class='col-lg-8'>
        <h3>
            <?php foreach ($Details->gamedevs as $developer) {
                echo Html::a($developer->developer->Name . ' ', ['developer/datastream', 'q' => $developer->developer->Name]);
            } ?>
        </h3>
    </div>
    
    
    <hr>
</div>