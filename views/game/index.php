<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Game', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Name',
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Publisher',
                'label' => 'Publisher',
                'content' => function($model, $key, $index, $column){
                        return $model->publisher->Name;
                   },
            ],
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Release_Date',
                'label' => 'Release Date',
                'content' => function($model, $key, $index, $column){
                        return date('d F, Y H:i',strtotime($model->Release_Date));
                   },
            ],
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Embargo_Date',
                'label' => 'Embargo Date',
                'content' => function($model, $key, $index, $column){
                        return date('d F, Y H:i',strtotime($model->Embargo_Date));
                   },
            ],
            [
                'class' => yii\grid\DataColumn::className(),
                'attribute' => 'Media_Date',
                'label' => 'Media Date',
                'content' => function($model, $key, $index, $column){
                        return date('d F, Y H:i',strtotime($model->Media_Date));
                   },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>