<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Game */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'Name',
            [
                'label' => 'Publisher',
                'value' => $model->publisher->Name,
            ],
            [
                'label' => 'Release Date',
                'value' => date('d F, Y H:i',strtotime($model->Release_Date)),
            ],
            [
                'label' => 'Embargo Date',
                'value' => date('d F, Y H:i',strtotime($model->Embargo_Date)),
            ],
            [
                'label' => 'Media Date',
                'value' => date('d F, Y H:i',strtotime($model->Media_Date)),
            ],
        ],
    ]) ?>

</div>
