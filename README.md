Game Embargo Tracker
================================

A web application that tracks the differences between a game's release date and its review embargo date.

Developed in response to the less than upright actions of companies within the game industry that abuse review embargoes to gain sales for games that would have reviewed terribly.

Developed in Yii Framework 2, using the Bootstrap 3 visual framework.