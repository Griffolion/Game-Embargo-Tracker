<?php

namespace app\controllers;

use Yii;
use app\models\Game;
use app\models\GameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['getgames', 'getchilddata', 'datastream'],
                        'allow' => true
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'getchilddata' => ['post'],
                    //'datastream' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Game models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Game model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Game model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Game();
        if ($model->load(Yii::$app->request->post())) {
            if (null === $model->findPublisherByName($model->Publisher)) {
                $model->addError('Publisher', 'Publisher not found');
            } else {
                $model->Publisher = $model->findPublisherByName($model->Publisher)->ID;
                if (NULL !== Game::findOne(['Name' => $model->Name, 'Publisher' => $model->Publisher])) {
                    return $this->redirect(['view', 'id' => Game::findOne(['Name' => $model->Name, 'Publisher' => $model->Publisher])->ID]);
                }
                if (!$model->hasErrors() && $model->save(true)) {
                    return $this->redirect(['view', 'id' => $model->ID]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            } 
        } else {
            return $this->render('create', [
                        'model' => $model,
                    ]);
        }
    }

    /**
     * Updates an existing Game model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Game model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDatastream()
    {
        $Details = NULL;
        if (isset($_POST['q'])) {
            $Details = Game::find()->where(['Name' => $_POST['q']])->one();
        }
        return $this->render('datastream', [
            'Details' => $Details,
        ]);
    }

    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    // returns JSON for typeahead widget in views/datastream/game
    public function actionGetgames($q = null)
    {
        return Json::encode(Game::getDataForJson($q));
    }
    
    public function actionGetchilddata($data = null)
    {   
        if (isset($_POST['data'])) {
            $Data = Game::find()->where(['Name' => $_POST['data']])->one();
            return $this->renderAjax('_details', [
                'Details' => $Data
            ]);
        }
        else {
            return '<h1>ERROR</h1>';
        }
    }
}
