<?php

namespace app\controllers;

use Yii;
use app\models\Gamedev;
use app\models\GamedevSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GamedevController implements the CRUD actions for Gamedev model.
 */
class GamedevController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gamedev models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GamedevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gamedev model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gamedev model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gamedev();

        if ($model->load(Yii::$app->request->post())) {
            $gameID = $model->findGameByName($model->Game);
            $devID = $model->findDeveloperByName($model->Developer);
            if (NULL === $gameID || NULL === $devID) {
                if (NULL === $gameID) {
                    $model->addError('Game', 'Game not found');
                }
                if (NULL === $devID) {
                    $model->addError('Developer', 'Developer not found');
                }
            } else {
                $model->Game = $gameID->ID;
                $model->Developer = $devID->ID;
            }
            if (NULL !== $model->findOne(['Developer' => $model->Developer, 'Game' => $model->Game])) {
                return $this->redirect(['view', 'id' => $model->findOne(['Developer' => $model->Developer, 'Game' => $model->Game])->ID]);
            }
            if (!$model->hasErrors() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Gamedev model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gamedev model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gamedev model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gamedev the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gamedev::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
