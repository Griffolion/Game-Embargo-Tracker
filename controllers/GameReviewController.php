<?php

namespace app\controllers;

use Yii;
use app\models\GameReview;
use app\models\GameReviewSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * GameReviewController implements the CRUD actions for GameReview model.
 */
class GameReviewController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GameReview models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GameReview model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GameReview model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GameReview();

        if ($model->load(Yii::$app->request->post())) {
            $gameID = $model->findGameByName($model->Game);
            $revID = $model->findReviewerByName($model->Reviewer);
            $platID = $model->findPlatformByName($model->Platform);
            if ($gameID === NULL || $revID === NULL || $platID === NULL) {
                if ($gameID === NULL) {
                    $model->addError('Game', 'Game not found');
                }
                if ($revID ===  NULL) {
                    $model->addError('Reviewer', 'Reviewer not found');
                }
                if ($platID ===  NULL) {
                    $model->addError('Platform', 'Platform not found');
                }
            } else {
                $model->Game = $gameID->ID;
                $model->Reviewer = $revID->ID;
                $model->Platform = $platID->ID;
            }
            if (NULL !== $model->findOne(['Platform' => $model->Platform, 'Game' => $model->Game, 'Reviewer' => $model->Reviewer])) {
                return $this->redirect(['view', 'id' => $model->findOne(['Platform' => $model->Platform, 'Game' => $model->Game, 'Reviewer' => $model->Reviewer])->ID]);
            }
            if (!$model->hasErrors() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GameReview model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GameReview model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GameReview model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GameReview the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GameReview::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
