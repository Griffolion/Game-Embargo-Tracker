<?php

namespace app\controllers;

use Yii;
use app\models\Gameplatform;
use app\models\GameplatformSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameplatformController implements the CRUD actions for Gameplatform model.
 */
class GameplatformController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gameplatform models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameplatformSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gameplatform model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gameplatform model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gameplatform();
        if ($model->load(Yii::$app->request->post())) {
            $gameID = $model->findGameByName($model->Game);
            $platID = $model->findPlatformByName($model->Platform);
            if (null === $gameID || null === $platID) {
                if (null === $model->findGameByName($model->Game)) {
                    $model->addError('Game', 'Game not found');
                }
                if (null ===  $model->findPlatformByName($model->Platform)) {
                    $model->addError('Platform', 'Platform not found');
                }
            } else {
                $model->Game = $gameID->ID;
                $model->Platform = $platID->ID;
            }
            if (NULL !== Gameplatform::findOne(['Game' => $model->Game, 'Platform' => $model->Platform])) {
                return $this->redirect(['view', 'id' => Gameplatform::findOne(['Game' => $model->Game, 'Platform' => $model->Platform])->ID]);
            }
            if (!$model->hasErrors() && $model->save(true)) {
                return $this->redirect(['view', 'id' => $model->ID]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                    ]);
        }
    }

    /**
     * Updates an existing Gameplatform model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gameplatform model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gameplatform model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gameplatform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gameplatform::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
