<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Gameplatform;

/**
 * GameplatformSearch represents the model behind the search form about `app\models\Gameplatform`.
 */
class GameplatformSearch extends Gameplatform
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Game', 'Platform'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gameplatform::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'Game' => $this->Game,
            'Platform' => $this->Platform,
        ]);

        return $dataProvider;
    }
}
