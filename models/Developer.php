<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "developer".
 *
 * @property integer $ID
 * @property string $Name
 *
 * @property Gamedev[] $gamedevs
 */
class Developer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'developer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGamedevs()
    {
        return $this->hasMany(Gamedev::className(), ['Developer' => 'ID']);
    }
    
    public function getDataForJson($q = null)
    {
        $out = [];
        if ($q === null) {
            $data = Developer::findAll('');
            foreach ($data as $datum) {
                $out[] = array('developer' => $datum->Name);
            }
        } else {
            $query = new Query;
            $query->select('Name')->from('developer')->where('Name LIKE "%' . $q . '%"')->orderBy('Name');
            $data = $query->createCommand()->queryAll();
            foreach ($data as $datum) {
                $out[] = array('developer' => $datum['Name']);
            }
        }
        return $out;
    }
}
