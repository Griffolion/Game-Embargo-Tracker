<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gamedev".
 *
 * @property integer $ID
 * @property integer $Game
 * @property integer $Developer
 *
 * @property Developer $developer
 * @property Game $game
 */
class Gamedev extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gamedev';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Game', 'Developer'], 'required'],
            //[['Game', 'Developer'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Game' => 'Game',
            'Developer' => 'Developer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(Developer::className(), ['ID' => 'Developer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::className(), ['ID' => 'Game']);
    }
    
    public function findGameByName($searchKey)
    {
        return Game::findOne(['Name' => $searchKey]);
    }
    
    public function findDeveloperByName($searchKey)
    {
        return Developer::findOne(['Name' => $searchKey]);
    }
}
