<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "publisher".
 *
 * @property integer $ID
 * @property string $Name
 *
 * @property Game[] $games
 */
class Publisher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['Publisher' => 'ID']);
    }
    
    public function getDataForJson($q = null)
    {
        $out = [];
        if ($q === null) {
            $data = Publisher::findAll('');
            foreach ($data as $datum) {
                $out[] = array('publisher' => $datum->Name);
            }
        } else {
            $query = new Query;
            $query->select('Name')->from('publisher')->where('Name LIKE "%' . $q . '%"')->orderBy('Name');
            $data = $query->createCommand()->queryAll();
            foreach ($data as $datum) {
                $out[] = array('publisher' => $datum['Name']);
            }
        }
        return $out;
    }
}
