<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    public $passwordVerify;
    public $captcha;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', 'passwordVerify', 'captcha'], 'required'],
            // username and password lengths are 255
            [['username', 'password', 'passwordVerify'], 'string', 'max' => 255],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // username is unique
            ['username', 'uniqueUsername'],
            //captcha verification
            ['captcha', 'captcha'],
            // verify password matches second input
            [['password'], 'verify']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'password' => 'Password',
            'passwordVerify' => 'Verify Password',
            'captcha' => 'Captcha'
        ];
    }
    
    public function uniqueUsername($attribute, $params)
    {
        // if User model is able to find a User with the same username as the one submitted
        if (\app\models\User::findByUsername($this->username) != null) {
            $this->addError($attribute, 'This username is taken');
        }
    }
    
    public function verify($attribute, $params) {
        if ($this->password !== $this->passwordVerify) {
            $this->addError('passwordVerify', 'Password not verified, check for typos');
        }
    }

    /**
     * Validates the password according to rules
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        // password is 8 to 20 characters in length
        if (!(strlen(utf8_decode($this->password)) >= 8 && strlen(utf8_decode($this->password)) <= 20)) {
            $this->addError($attribute, 'Password must be 8 to 20 characters long');
        }
        // password has at least one 0-9 digit
        if (!preg_match('/\d{1,}/', $this->password)) {
            $this->addError($attribute, 'Password must contain at least one digit');
        }
        // password has at least one lower case character (a-z)
        if (!preg_match('/[a-z]{1,}/', $this->password)) {
            $this->addError($attribute, 'Password must contain at least one lower case character');
        }
        // password has at least one upper case character (A-Z)
        if (!preg_match('/[A-Z]{1,}/', $this->password)) {
            $this->addError($attribute, 'Password must contain at least one upper case character');
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function register()
    {
        if ($this->validate()) {
            \Yii::$app->getSecurity()->passwordHashStrategy = 'password_hash';
            $u = new User();
            $u->username = $this->username;
            $u->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $u->access_token = \Yii::$app->getSecurity()->generateRandomString();
            $u->auth_key = \Yii::$app->getSecurity()->generateRandomString();
            if ($u->validate()) {
                $u->save(false);
                return true;
            }
            else {
                throw new HttpException(500, 'User class did not validate');
            }
        }
        return false;
    }
}
