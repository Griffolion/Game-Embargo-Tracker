<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gameplatform".
 *
 * @property integer $ID
 * @property integer $Game
 * @property integer $Platform
 *
 * @property Game $game
 * @property Platform $platform
 */
class Gameplatform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gameplatform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Game', 'Platform'], 'required'],
            //[['Game', 'Platform'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Game' => 'Game',
            'Platform' => 'Platform',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::className(), ['ID' => 'Game']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['ID' => 'Platform']);
    }
    
    public function findGameByName($searchKey)
    {
        return Game::findOne(['Name' => $searchKey]);
    }
    
    public function findPlatformByName($searchKey)
    {
        return Platform::findOne(['Name' => $searchKey]);
    }
}
