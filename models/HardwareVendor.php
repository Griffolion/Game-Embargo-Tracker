<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "hardwarevendor".
 *
 * @property integer $ID
 * @property string $Name
 *
 * @property Platform[] $platforms
 */
class HardwareVendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hardwarevendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatforms()
    {
        return $this->hasMany(Platform::className(), ['Hardware_Vendor' => 'ID']);
    }
    
    public function getDataForJson($q = null)
    {
        if ($q === null) {
            $data = HardwareVendor::findAll('');
            if (empty($data)) {
                return array('value' => 'No Data');
            } else {
                $u = array();
                foreach ($data as $datum) {
                    $u[] = ['value' => $datum->Name];
                }
                return $u;
            }
        } else {
            $query = new Query;
            $query->select('Name')->from('hardwarevendor')->where('Name LIKE "%' . $q . '%"')->orderBy('Name');
            $data = $query->createCommand()->queryAll();
            if (empty($data)) {
                return array('value' => 'No Data');
            } else {
                $out = [];
                foreach ($data as $datum) {
                    $out[] = array('value' => $datum['Name']);
                }
                return $out;
            }
        }
    }
}
