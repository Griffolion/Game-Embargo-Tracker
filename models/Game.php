<?php

namespace app\models;

use Yii;
use yii\db\Query;
use app\models\Game;

/**
 * This is the model class for table "game".
 *
 * @property integer $ID
 * @property integer $Publisher
 * @property string $Name
 * @property string $Release_Date
 * @property string $Embargo_Date
 * @property string $Media_Date
 *
 * @property Publisher $publisher
 * @property Gamedev[] $gamedevs
 * @property Gamereview[] $gamereviews
 * @property Gameplatform[] $gameplatforms
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Publisher', 'Name'], 'required'],
            //[['Publisher'], 'integer'],
            [['Name'], 'string', 'max' => 256],
            [['Release_Date', 'Embargo_Date', 'Media_Date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Publisher' => 'Publisher',
            'Name' => 'Name',
            'Release_Date' => 'Release Date',
            'Embargo_Date' => 'Embargo Date',
            'Media_Date' => 'Media Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['ID' => 'Publisher']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGamedevs()
    {
        return $this->hasMany(Gamedev::className(), ['Game' => 'ID']);
    }
    
    public function getGamereviews()
    {
        return $this->hasMany(Gamereview::className(), ['Game' => 'ID']);
    }
    
    public function getGameplatforms()
    {
        return $this->hasMany(Gameplatform::className(), ['Game' => 'ID']);
    }
    
    public function getDataForJson($q = null)
    {
        $out = [];
        if ($q === null) {
            $data = Game::findAll('');
            foreach ($data as $datum) {
                $out[] = array('game' => $datum->Name);
            }
        } else {
            $query = new Query;
            $query->select('Name')->from('game')->where('Name LIKE "%' . $q . '%"')->orderBy('Name');
            $data = $query->createCommand()->queryAll();
            foreach ($data as $datum) {
                $out[] = array('game' => $datum['Name']);
            }
        }
        return $out;
    }
    
    public function findPublisherByName($searchKey)
    {
        return Publisher::findOne(['Name' => $searchKey]);
    }
}