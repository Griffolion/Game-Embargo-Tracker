<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Gamedev;

/**
 * GamedevSearch represents the model behind the search form about `app\models\Gamedev`.
 */
class GamedevSearch extends Gamedev
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Game', 'Developer'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gamedev::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'Game' => $this->Game,
            'Developer' => $this->Developer,
        ]);

        return $dataProvider;
    }
}
