<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "reviewer".
 *
 * @property integer $ID
 * @property string $Name
 *
 * @property Gamereview[] $gamereviews
 */
class Reviewer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviewer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGamereviews()
    {
        return $this->hasMany(Gamereview::className(), ['Reviewer' => 'ID']);
    }
    
    public function getDataForJson($q = null)
    {
        if ($q === null) {
            $data = Reviewer::findAll('');
            if (empty($data)) {
                return array('value' => 'No Data');
            } else {
                $u = array();
                foreach ($data as $datum) {
                    $u[] = ['value' => $datum->Name];
                }
                return $u;
            }
        } else {
            $query = new Query;
            $query->select('Name')->from('reviewer')->where('Name LIKE "%' . $q . '%"')->orderBy('Name');
            $data = $query->createCommand()->queryAll();
            if (empty($data)) {
                return array('value' => 'No Data');
            } else {
                $out = [];
                foreach ($data as $datum) {
                    $out[] = array('value' => $datum['Name']);
                }
                return $out;
            }
        }
    }
}
