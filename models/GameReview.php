<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gamereview".
 *
 * @property integer $ID
 * @property integer $Reviewer
 * @property integer $Game
 * @property integer $Platform
 * @property integer $Score
 * @property string $Summary
 *
 * @property Reviewer $reviewer
 * @property Game $game
 * @property Platform $platform
 */

class GameReview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gamereview';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Reviewer', 'Game', 'Platform'], 'required'],
            [['Score'], 'integer'],
            [['Summary'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Reviewer' => 'Reviewer',
            'Game' => 'Game',
            'Platform' => 'Platform',
            'Score' => 'Score',
            'Summary' => 'Summary',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewer()
    {
        return $this->hasOne(Reviewer::className(), ['ID' => 'Reviewer']);
    }
    
    public function getGame()
    {
        return $this->hasOne(Game::className(), ['ID' => 'Game']);
    }
    
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['ID' => 'Platform']);
    }
    
    public function findGameByName($searchKey)
    {
        return Game::findOne(['Name' => $searchKey]);
    }
    
    public function findPlatformByName($searchKey)
    {
        return Platform::findOne(['Name' => $searchKey]);
    }
    
    public function findReviewerByName($searchKey)
    {
        return Reviewer::findOne(['Name' => $searchKey]);
    }
}
