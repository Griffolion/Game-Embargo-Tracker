<?php

namespace app\models;

use Yii;
use app\models\Platform;
use yii\db\Query;

/**
 * This is the model class for table "platform".
 *
 * @property integer $ID
 * @property integer $Hardware_Vendor
 * @property string $Name
 *
 * @property Hardwarevendor $hardwareVendor
 * @property Gamereview[] $gamereviews
 * @property Gameplatform[] $gameplatforms
 */

class Platform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'platform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Hardware_Vendor'], 'required'],
            //[['Hardware_Vendor'], 'integer'],
            [['Name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Hardware_Vendor' => 'Hardware  Vendor',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHardwareVendor()
    {
        return $this->hasOne(Hardwarevendor::className(), ['ID' => 'Hardware_Vendor']);
    }
    
    public function getGamereviews()
    {
        return $this->hasMany(Gamereview::className(), ['Platform' => 'ID']);
    }
    
    public function getGameplatforms()
    {
        return $this->hasMany(Gameplatform::className(), ['Platform' => 'ID']);
    }
    
    public function getDataForJson($q = null)
    {
        $out = [];
        if ($q === null) {
            $data = Platform::findAll('');
            foreach ($data as $datum) {
                $out[] = array('platform' => $datum->Name);
            }
        } else {
            $query = new Query;
            $query->select('Name')->from('platform')->where('Name LIKE "%' . $q . '%"')->orderBy('Name');
            $data = $query->createCommand()->queryAll();
            foreach ($data as $datum) {
                $out[] = array('platform' => $datum['Name']);
            }
        }
        return $out;
    }
    
    public function findHardwareVendorByName($searchKey)
    {
        return HardwareVendor::findOne(['Name' => $searchKey]);
    }
}