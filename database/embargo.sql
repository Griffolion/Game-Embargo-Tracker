-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 08:41 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `embargo`
--

-- --------------------------------------------------------

--
-- Table structure for table `developer`
--

CREATE TABLE IF NOT EXISTS `developer` (
`ID` int(11) NOT NULL,
  `Name` varchar(256) DEFAULT 'Unnamed Developer'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
`ID` int(11) NOT NULL,
  `Publisher` int(11) NOT NULL,
  `Name` varchar(256) DEFAULT NULL,
  `Release_Date` datetime(6) DEFAULT NULL,
  `Embargo_Date` datetime(6) DEFAULT NULL,
  `Media_Date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `gamedev`
--

CREATE TABLE IF NOT EXISTS `gamedev` (
`ID` int(11) NOT NULL,
  `Game` int(11) NOT NULL,
  `Developer` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `gameplatform`
--

CREATE TABLE IF NOT EXISTS `gameplatform` (
`ID` int(11) NOT NULL,
  `Game` int(11) NOT NULL,
  `Platform` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `gamereview`
--

CREATE TABLE IF NOT EXISTS `gamereview` (
`ID` int(11) NOT NULL,
  `Reviewer` int(11) NOT NULL,
  `Game` int(11) NOT NULL,
  `Platform` int(11) NOT NULL,
  `Score` int(11) DEFAULT NULL,
  `Summary` longtext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `hardwarevendor`
--

CREATE TABLE IF NOT EXISTS `hardwarevendor` (
`ID` int(11) NOT NULL,
  `Name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE IF NOT EXISTS `platform` (
`ID` int(11) NOT NULL,
  `Hardware_Vendor` int(11) NOT NULL,
  `Name` varchar(256) DEFAULT 'Unnamed Platform'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

CREATE TABLE IF NOT EXISTS `publisher` (
`ID` int(11) NOT NULL,
  `Name` varchar(256) DEFAULT 'Unnamed Publisher'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `reviewer`
--

CREATE TABLE IF NOT EXISTS `reviewer` (
`ID` int(11) NOT NULL,
  `Name` varchar(256) DEFAULT 'Unnamed Reviewer'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `access_token` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `developer`
--
ALTER TABLE `developer`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_Game_Publisher1_idx` (`Publisher`);

--
-- Indexes for table `gamedev`
--
ALTER TABLE `gamedev`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_GameDev_Game1_idx` (`Game`), ADD KEY `fk_GameDev_Developer1_idx` (`Developer`);

--
-- Indexes for table `gameplatform`
--
ALTER TABLE `gameplatform`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_gamePlatform_Game1_idx` (`Game`), ADD KEY `fk_gamePlatform_Platform1_idx` (`Platform`);

--
-- Indexes for table `gamereview`
--
ALTER TABLE `gamereview`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_GameReview_Reviewer1_idx` (`Reviewer`), ADD KEY `fk_GameReview_Game1_idx` (`Game`), ADD KEY `fk_GameReview_Platform1_idx` (`Platform`);

--
-- Indexes for table `hardwarevendor`
--
ALTER TABLE `hardwarevendor`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `platform`
--
ALTER TABLE `platform`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_Platform_HardwareVendor1_idx` (`Hardware_Vendor`);

--
-- Indexes for table `publisher`
--
ALTER TABLE `publisher`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `reviewer`
--
ALTER TABLE `reviewer`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `developer`
--
ALTER TABLE `developer`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gamedev`
--
ALTER TABLE `gamedev`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gameplatform`
--
ALTER TABLE `gameplatform`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gamereview`
--
ALTER TABLE `gamereview`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hardwarevendor`
--
ALTER TABLE `hardwarevendor`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `platform`
--
ALTER TABLE `platform`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `publisher`
--
ALTER TABLE `publisher`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reviewer`
--
ALTER TABLE `reviewer`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `game`
--
ALTER TABLE `game`
ADD CONSTRAINT `fk_Game_Publisher1` FOREIGN KEY (`Publisher`) REFERENCES `publisher` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gamedev`
--
ALTER TABLE `gamedev`
ADD CONSTRAINT `fk_GameDev_Developer1` FOREIGN KEY (`Developer`) REFERENCES `developer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_GameDev_Game1` FOREIGN KEY (`Game`) REFERENCES `game` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gameplatform`
--
ALTER TABLE `gameplatform`
ADD CONSTRAINT `fk_gamePlatform_Game1` FOREIGN KEY (`Game`) REFERENCES `game` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_gamePlatform_Platform1` FOREIGN KEY (`Platform`) REFERENCES `platform` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gamereview`
--
ALTER TABLE `gamereview`
ADD CONSTRAINT `fk_GameReview_Game1` FOREIGN KEY (`Game`) REFERENCES `game` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_GameReview_Platform1` FOREIGN KEY (`Platform`) REFERENCES `platform` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_GameReview_Reviewer1` FOREIGN KEY (`Reviewer`) REFERENCES `reviewer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `platform`
--
ALTER TABLE `platform`
ADD CONSTRAINT `fk_Platform_HardwareVendor1` FOREIGN KEY (`Hardware_Vendor`) REFERENCES `hardwarevendor` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
